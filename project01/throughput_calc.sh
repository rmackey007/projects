#Non-Forking

cat large_throughput.dat | awk '{ sum+=$1 } 
	{ count++ }
	END { print sum/count"\tlarge" } ' >> out

cat medium_throughput.dat | awk '{ sum+=$1 } 
	{ count++ }
	END { print sum/count"\tmedium" } ' >> out

cat small_throughput.dat | awk '{ sum+=$1 } 
	{ count++ }
	END { print sum/count"\tsmall" } ' >> out

cat out | nl > through.dat
