#Forking

cat fork_large_throughput.dat | awk '{ sum+=$1 } 
	{ count++ }
	END { print sum/count"\tlarge" } ' >> output

cat fork_medium_throughput.dat | awk '{ sum+=$1 } 
	{ count++ }
	END { print sum/count"\tmedium" } ' >> output

cat fork_small_throughput.dat | awk '{ sum+=$1 } 
	{ count++ }
	END { print sum/count"\tsmall" } ' >> output

cat output | nl > through_fork.dat
