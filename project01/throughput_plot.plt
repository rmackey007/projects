set terminal png size 400,300
set output 'throughput.png'
set title "Throughput"
set xlabel "File Size"
set ylabel "Throughput (KB/sec)"
Forking = "#99ffff"; NonForking = "#4671d5";
set auto x
set yrange [0:11000]
set style data histogram
set style histogram cluster gap 1
set style fill solid border -1
set boxwidth 0.9
set xtic scale 0
plot 'through.dat' using 2:xtic(3) title "Non-Forking", \
'through_fork.dat' using 2:xtic(3) title "Forking"
