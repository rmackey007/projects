#!/bin/sh

# Script latency
./thor.py -p 10 -r 10 http://student01.cse.nd.edu:9667/www/cgi-bin/hello.sh | grep Average | cut -d : -f 2 | sed -e 's/^\s*//' >> script_latency_fork.dat

# File latency
./thor.py -p 10 -r 10 http://student01.cse.nd.edu:9667/www/songs/1000.txt | grep Average | cut -d : -f 2 | sed -e 's/^\s*//' >> file_latency_fork.dat

# Directory latency
./thor.py -p 10 -r 10 http://student01.cse.nd.edu:9667/www/cgi-bin/ | grep Average | cut -d : -f 2 | sed -e 's/^\s*//' >> dir_latency_fork.dat
