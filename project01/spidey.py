#!/usr/bin/env python2.7

import getopt
import logging
import os
import socket
import sys
import time
import datetime
import mimetypes
import signal

# Constants

ADDRESS  = '0.0.0.0'
PORT     = 9234
FORK     = False
BACKLOG  = 0
LOGLEVEL = logging.INFO
PROGRAM  = os.path.basename(sys.argv[0])
DOCROOT  = '.'
# Utility Functions

def usage(exit_code=0):
    print >>sys.stderr, '''Usage: {program} [-p PORT -v -f -d DOCROOT]

Options:

    -h       Show this help message
    -v       Set logging to DEBUG level

    -d DOCROOT  Set directory to display (default is current directory)
    -p PORT  TCP Port to listen to (default is {port})
    -f       Enable forking mode
'''.format(port=PORT, program=PROGRAM)
    sys.exit(exit_code)

# BaseHandler Class

class BaseHandler(object):

    def __init__(self, fd, address):
        ''' Construct handler from file descriptor and remote client address '''
        self.logger  = logging.getLogger()        # Grab logging instance
        self.socket  = fd                         # Store socket file descriptor
        self.address = '{}:{}'.format(*address)   # Store address
        self.stream  = self.socket.makefile('w+') # Open file object from file descriptor

        self.debug('Connect')

    def debug(self, message, *args):
        ''' Convenience debugging function '''
        message = message.format(*args)
        self.logger.debug('{} | {}'.format(self.address, message))

    def info(self, message, *args):
        ''' Convenience information function '''
        message = message.format(*args)
        self.logger.info('{} | {}'.format(self.address, message))

    def warn(self, message, *args):
        ''' Convenience warning function '''
        message = message.format(*args)
        self.logger.warn('{} | {}'.format(self.address, message))

    def error(self, message, *args):
        ''' Convenience error function '''
        message = message.format(*args)
        self.logger.error('{} | {}'.format(self.address, message))

    def exception(self, message, *args):
        ''' Convenience exception function '''
        message = message.format(*args)
        self.logger.error('{} | {}'.format(self.address, message))

    def handle(self):
        ''' Handle connection '''
        self.debug('Handle')
        raise NotImplementedError

    def finish(self):
        ''' Finish connection by flushing stream, shutting down socket, and
        then closing it '''
        self.debug('Finish')
        try:
            self.stream.flush()
            self.socket.shutdown(socket.SHUT_RDWR)
        except socket.error as e:
            pass    # Ignore socket errors
        finally:
            self.socket.close()

# HTTPHandler Class

class HTTPHandler(BaseHandler):
    def __init__(self, fd, address, docroot=None):
        BaseHandler.__init__(self, fd, address)
        self.docroot = DOCROOT

    def handle(self):
        ''' Handle connection by determining the nature of the request and creating a socket '''
        self.debug('Handle')

        data = self.stream.readline().rstrip()

        if 'favicon' not in data:
            print data

            DIRE = self._parse_request(data)

            if not os.path.exists(os.environ.get("REQUEST_URI")):
                self._handle_error('404')
            else:
                if DIRE:
                    self._handle_directory()
                else:
                    if os.environ.get("REQUEST_URI")[-3:] == '.sh' and os.access(os.environ["REQUEST_URI"], os.X_OK):
                        self._handle_script()
                    elif os.access(os.environ["REQUEST_URI"], os.R_OK):
                        self._handle_file(os.environ.get("REQUEST_URI"))
                    else:
                        self._handle_error('403')    

            self.stream.flush()

    def _parse_request(self, data):
        if 'favicon' in data:
            return

        if 'GET' in data:
            data = data.split()
            self.debug('Parsing Request: {}'.format(data))
            os.environ["REQUEST_URI"] = os.getcwd() + data[1].split('?')[0]

        if '?' in data[1]:
            os.environ["QUERY_STRING"] = data[1].split('?')[1]

        if os.path.isdir(os.environ.get("REQUEST_URI")):
            return True
        else:
            os.environ["REQUEST_URI"] = os.environ.get("REQUEST_URI")[:-1]
            return False

    def _handle_file(self, file):
        mimetype, _ = mimetypes.guess_type(os.environ["REQUEST_URI"])
        if mimetype is None:
            mimetype = 'application/octet-stream'
        self.stream.write('HTTP/1.0 200 OK\r\n')
        self.stream.write('Content-Type: {}\r\n'.format(mimetype))
        self.stream.write('\r\n')
        for line in open(file):
            self.stream.write(line)

    def _handle_directory(self):
        self.stream.write('HTTP/1.0 200 OK\r\n')
        self.stream.write('Content-Type: text/html\r\n')
        self.stream.write('\r\n')
        files = sorted(os.listdir(os.environ.get("REQUEST_URI")))
        self.stream.write('<table border="0" width="600">')
        self.stream.write('<tr><td><b>Name</td><td><b>Modification Time</td><td><b>Size</td></tr>')
        for item in files:
            path = os.path.join(os.environ["REQUEST_URI"], item)
            self.stream.write('<tr>')
            self.stream.write('<td>')
            self.stream.write('<a href="{}/">{}</a>'.format(os.path.normpath(item), item))
            self.stream.write('</td>')
            self.stream.write('<td>')
            self.stream.write('{}'.format(datetime.datetime.fromtimestamp(os.path.getmtime(path))))
            self.stream.write('</td>')
            self.stream.write('<td>')
            self.stream.write(os.path.getsize(path))
            self.stream.write('</td>')
            self.stream.write('</tr>')
        
        self.stream.write('</table>')
        
    def _handle_script(self):
        signal.signal(signal.SIGCHLD, signal.SIG_DFL)
        for line in os.popen(os.environ.get("REQUEST_URI")):
            self.stream.write(line)
        signal.signal(signal.SIGCHLD, signal.SIG_IGN)

    def _handle_error(self, error):

        self.stream.write('HTTP/1.0 {} Error\r\n'.format(error))
        self.stream.write('Content-Type: text/html\r\n')
        self.stream.write('\r\n')
        if error == '404':
            self.stream.write('<h1>404 Error</h1><br><img src="http://i.imgur.com/3yCJ7Lo.jpg">')
        if error == '403':
            self.stream.write('<h1>403 Error</h1><br><br><img src="https://media.giphy.com/media/10WzOCTS7DTWOA/giphy.gif">')

# TCPServer Class

class TCPServer(object):

    def __init__(self, address=ADDRESS, port=PORT, handler=HTTPHandler):
        ''' Construct TCPServer object with the specified address, port, and
        handler '''
        self.logger  = logging.getLogger()                              # Grab logging instance
        self.socket  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)# Allocate TCP socket
        self.address = address                                          # Store address to listen on
        self.port    = port                                             # Store port to lisen on
        self.handler = handler                                          # Store handler for incoming connections

    def run(self):
        ''' Run TCP Server on specified address and port by calling the
        specified handler on each incoming connection '''
        try:
            # Bind socket to address and port and then listen
            self.socket.bind((self.address, self.port))
            self.socket.listen(BACKLOG)
        except socket.error as e:
            self.logger.error('Could not listen on {}:{}: {}'.format(self.address, self.port, e))
            sys.exit(1)

        self.logger.info('Listening on {}:{}...'.format(self.address, self.port))

        while True:
            # Accept incoming connection
            client, address = self.socket.accept()
            self.logger.debug('Accepted connection from {}:{}'.format(*address))

            if not FORK:
            # Instantiate handler, handle connection, finish connection
                try:
                    handler = self.handler(client, address, DOCROOT)
                    handler.handle()
                except Exception as e:
                    handler.exception('Exception: {}', e)
                finally:
                    handler.finish()
            else:
                pid = os.fork()
                if pid == 0:
                    try:
                        handler = self.handler(client, address, DOCROOT)
                        handler.handle()
                    except Exception as e:
                        handler.exception('Exception: {}', e)
                    finally:
                        handler.finish()
                    sys.exit(0)
                else:
                    client.close()
                    
# Main Execution

if __name__ == '__main__':
    # Parse command-line arguments

    signal.signal(signal.SIGCHLD, signal.SIG_IGN)
    try:
        options, arguments = getopt.getopt(sys.argv[1:], "hp:vd:f")
    except getopt.GetoptError as e:
        usage(1)

    for option, value in options:
        if option == '-p':
            PORT = int(value)
        elif option == '-v':
            LOGLEVEL = logging.DEBUG
        elif option == '-d':
            DOCROOT = value
        elif option == '-f':
            FORK = True
        elif option == '-h':
            usage(0)
        else:
            usage(1)

    # Set logging level
    logging.basicConfig(
        level   = LOGLEVEL,
        format  = '[%(asctime)s] %(message)s',
        datefmt = '%Y-%m-%d %H:%M:%S',
    )

    # Instantiate and run server
    server = TCPServer(port=PORT)

    try:
        server.run()
    except KeyboardInterrupt:
        sys.exit(0)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
