#!/usr/bin/env python2.7

import os
import socket
import sys

# Constants

ADDRESS = '0.0.0.0'
PORT    = 9234
BACKLOG = 0
PROGRAM = os.path.basename(sys.argv[0])

# Echo Server

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # Allocate TCP socket
server.bind((ADDRESS, PORT))                                # Bind to address and port
server.listen(BACKLOG)                                      # Listen on socket

while True:
    client, address = server.accept()       # Accept incoming connection
    stream = client.makefile('w+')          # Create file object from socket

    data = stream.readline()                # Read from socket
    while data:
        sys.stdout.write(data)              # Write to STDOUT
        stream.write(data)                  # Write to socket
        stream.flush()                      # Flush socket
        data = stream.readline()            # Read from socket

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
