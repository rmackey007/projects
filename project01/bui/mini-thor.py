#!/usr/bin/env python2.7

import os
import socket
import sys
import time

# TCPClient class

class TCPClient(object):
    def __init__(self, address, port):
        self.address = socket.gethostbyname(address)
        self.port    = port
        self.socket  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def handle(self):
        self.stream.write('Hello\n')

    def run(self):
        self.socket.connect((self.address, self.port))
        self.stream = self.socket.makefile('w+')

        self.handle()
        self.finish()

    def finish(self):
        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()

class HTTPClient(TCPClient):
    def __init__(self, address, port, path):
        TCPClient.__init__(self, address, port)
        self.host = address
        self.path = path

    def handle(self):
        # Send request
        self.stream.write('GET {} HTTP/1.0\r\n'.format(self.path))
        self.stream.write('Host: {}\r\n'.format(self.host))
        self.stream.write('\r\n')
        self.stream.flush()

        # Receive response
        data = self.stream.readline()
        while data:
            #sys.stdout.write(data)
            data = self.stream.readline()


# Main execution

if __name__ == '__main__':
    for r in range(10):
        start_time = time.time()
        client = HTTPClient('example.com', 80, '/')
        client.run()
        end_time = time.time()
        print 'Elapsed time: {:0.2f}'.format(end_time - start_time)
