#!/usr/bin/env python2.7

import os
import socket
import sys
import time

# TCPServer class

class TCPServer(object):
    def __init__(self, address, port, handler):
        self.address = socket.gethostbyname(address)
        self.port    = port
        self.socket  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.handler = handler

    def run(self):
        self.socket.bind((self.address, self.port))
        self.socket.listen(0)

        while True:
            client, address = self.socket.accept()
            handler = self.handler(client, address)
            handler.handle()
            handler.finish()

class BaseHandler(object):

    def __init__(self, fd, address):
        self.socket = fd
        self.address = address
        self.stream = self.socket.makefile('w+')
    
    def handle(self):
        pass

    def finish(self):
        self.stream.flush()
        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()

class HTTPHandler(BaseHandler):
    def handle(self):
        data = self.stream.readline().rstrip()
        while data:
            sys.stdout.write(data + '\n')
            data = self.stream.readline().rstrip()

        self.stream.write('HTTP/1.0 200 OK\r\n')
        self.stream.write('Content-Type: text/html\r\n')
        self.stream.write('\r\n')
        '''
        self.stream.write('<h1>Hello, {}:{}</h1>'.format(self.address[0], self.address[1]))
        '''
        self._handle_file()

    def _handle_file(self):
        #self.stream.write('<pre>')
        for line in open(__file__):
            self.stream.write(line)
        #self.stream.write('</pre>')

# Main execution

if __name__ == '__main__':
    server = TCPServer('0.0.0.0', 9998, HTTPHandler)
    server.run()
