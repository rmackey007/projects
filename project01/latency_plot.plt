set terminal png size 400,300
set output 'latency.png'
set title "Latency"
set xlabel "Type"
set ylabel "Latency (sec)"
Forking = "#99ffff"; NonForking = "#4671d5";
set auto x
set yrange [0:0.110]
set style data histogram
set style histogram cluster gap 1
set style fill solid border -1
set boxwidth 0.9
set xtic scale 0
plot 'latency.dat' using 2:xtic(3) title "Non-Forking", \
'latency_fork.dat' using 2:xtic(3) title "Forking"
