#Non-Forking

cat file_latency_fork.dat | awk '{ sum+=$1 } 
	{ count++ }
	END { print sum/count"\tFile" } ' >> out

cat dir_latency_fork.dat | awk '{ sum+=$1 } 
	{ count++ }
	END { print sum/count"\tDirectory" } ' >> out

cat script_latency_fork.dat | awk '{ sum+=$1 } 
	{ count++ }
	END { print sum/count"\tScript" } ' >> out

cat out | nl > latency_fork.dat
