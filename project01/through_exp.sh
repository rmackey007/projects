#!/bin/sh

./thor.py -p 10 -r 10 http://student01.cse.nd.edu:9666/small.txt/ | grep Average | cut -d : -f 2 | sed -e 's/^\s*//' | awk '{tp = 1/($1+.001); print tp}' > small_throughput.dat

./thor.py -p 10 -r 10 http://student01.cse.nd.edu:9666/medium.txt/ | grep Average | cut -d : -f 2 | sed -e 's/^\s*//' | awk '{tp = 1000/$1; print tp}' > medium_throughput.dat

./thor.py -p 10 -r 10 http://student01.cse.nd.edu:9666/large.txt/ | grep Average | cut -d : -f 2 | sed -e 's/^\s*//' | awk '{tp = 1000000/$1; print tp}' > large_throughput.dat