\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{booktabs}
\usepackage{float}
\usepackage[margin=1.0in]{geometry}
\usepackage{hyperref}

\title{Project 01: Networking }
\author{Cat Badart & Ryan Mackey}
\date{April 20, 2016}

\begin{document}

\maketitle

\section*{Summary}
  In this project we wrote a client (\texttt{thor.py}) and a server (\texttt{spidey.py}).  We started out by following the examples provided to us with the \texttt{echo} examples and the \texttt{mini-} examples covered in class.  Initially we split \texttt{thor.py} into small tasks and worked on it individually.  Ryan worked on the URL parsing, verbose logging and forking while Cat conquered writing the classes and calculating the elapsed and average elapsed times.  This proved to be difficult to merge everything back together so we switched to partner programming to write \texttt{spidey.py}.  
  \newline
  \newline
  We are pretty sure that most of the project works, however while running our experiments we did get some Connection Errors (\texttt{104, 111}), but the scripts still executed and gave us data.  We suspect that we just overloaded the server in some way because these errors only occured when we ran the large (\texttt{1 GB}) file.
\section*{Latency}
  To measure the latency, we used \texttt{thor.py} to send requests to our server while \texttt{spidey.py} listened both in forking and single connection mode.  Our shell script \texttt{latency\_exp.sh} runs \texttt{thor.py -p 10 -r 10} 3 times while \texttt{spidey.py} is running in single connection mode on another machine; once each on a file, directory, and CGI script.  The results get passed through an extensive pipeline and redirected into \texttt{file\_latency\_results.dat}, \texttt{dir\_latency\_results.dat} and \texttt{script\_latency\_results.dat}  We then ran a slightly modified \texttt{latency\_exp.sh} again while \texttt{spidey.py} was in forking mode and generated 3 more data files with a \texttt{\_fork.dat} at the end instead.  This gave us 6 data files, each with 10 numbers in them.  The next step was to calculate the average of each data file and group them into 2 files, each with 3 numbers and labels.  \texttt{latency\_calc.sh} and \texttt{latency\_calc\_fork.sh} did this using \texttt{awk}:
  \newline
  \newline
  \texttt{latency\_calc.sh}
  \newline
  \newline
  \texttt{\indent cat file\_latency\_results.dat | awk '\{ sum+=\$1 \} }
  \newline
  \texttt{\indent \{ count++ \}}
  \newline
  \texttt{\indent END \{print sum/count"{\textbackslash}tFile" \} ' >> out}
  \newline
  \newline
  \texttt{\indent cat dir\_latency\_results.dat | awk '\{ sum+=\$1 \} }
  \newline
  \texttt{\indent \{ count++ \}}
  \newline
  \texttt{\indent END \{print sum/count"{\textbackslash}tDirectory" \} ' >> out}
  \newline
  \newline
  \texttt{\indent cat script\_latency\_results.dat | awk '\{ sum+=\$1 \} }
  \newline
  \texttt{\indent \{ count++ \}}
  \newline
  \texttt{\indent END \{print sum/count"{\textbackslash}tScript" \} ' >> out}
  \newline
  \newline
  \texttt{\indent cat out | nl > latency.dat }
  \newline
  \newline
  This script generated results (\texttt{latency.dat} and \texttt{latency\_fork.dat}) in the form of a TSV.  We then wrote \texttt{latency\_plot.plt} to generate a bar graph (\texttt{latency.png}) using these 2 data files.
  \newline
  \newline
  Figure ~\ref{fig:latency} contains a plot of our latency results as produced by \texttt{latency\_plot.plt}:
\begin{figure}[h!]
  \center \includegraphics[scale=1]{latency.png}
  \caption{Latency Results}
  \label{fig:latency}
\end{figure}
\section*{Throughput}
Measuring throughput was very similar to measuring latency, but with a few extra steps.  First we used \texttt{dd} to create files of 3 different sizes: small (\texttt{1 KB}), medium (\texttt{1 MB}) and large (\texttt{1 GB}).  \texttt{through\_exp.sh} used pretty much the same pipeline as \texttt{latency\_exp.sh} but we added \texttt{| awk '{tp = SIZE/(\$1); print tp}'} at the end (\texttt{SIZE} being either 1, 1000, or 1000000 because we decided to measure throughput as KB/sec and not bytes/sec).  After that it was the same deal as before, run the experiment while \texttt{spidey.py} is in single connection and then forking mode to generate 6 data files.  Again use calculation scripts (\texttt{throughput\_calc.sh} and \texttt{throughput\_calc\_fork.sh}) to generate 2 TSV files (\texttt{through.dat} and \texttt{through\_fork.dat}) that have the averages and labels.  \texttt{throughput\_plot.plt} uses those 2 files to generate \texttt{throughput.png}. 
\newline
\newline
Figure ~\ref{fig:throughput} contains a plot of our throughput results as produced by \texttt{throughput\_plot.plt}:
\begin{figure}[H]
  \center \includegraphics[scale=1]{throughput.png}
  \caption{Throughput Results}
  \label{fig:throughput}
\end{figure}
\section*{Analysis}
For our latency experiment, there was not a huge difference between forking and single connection mode.  Single connection was faster when dealing with files and scripts but slower with directories.  Even then, it was only about .01 seconds faster on average.  The real difference between forking and single connection was made very apparent when we did the throughput experiment.  Forking mode was much faster and could handle larger amounts of data than single connection.  In fact, when receiving requests for the large file in single connection mode, \texttt{spidey.py} would run for about 30 minutes and then crash before it finished handling all the requests (it usually got to around 20-30).  Whereas in forking mode, it only took about 3 minutes for \texttt{through\_exp.sh} to finish executing.  One important thing to note is that forking mode takes up more resources/processes.  We had to run \texttt{thor.py} and \texttt{spidey.py} on different machines when they were in forking mode.  So even though forking allows the server to handle more data and process requests faster, it takes a lot more resources, and for smaller files forking and single connection were not very different.  
\section*{Conclusions}
One very important lesson we learned is to always watch out for zombies!  We had issues with this in \texttt{spidey.py} and we learned the hard way (aka spent hours of agony) trying to fix this bug when all we needed to do was add 1 line of code in our \texttt{main} function (*facepalm*).  
We also had a nice little \texttt{html} refresher.  We used some basic \texttt{html} table tags to format how our directories were listed, but it had been a while since either of us had used it.  This project was also a really good way to practice pair programming, which is useful in a lot of our other classes.  
\end{document}

