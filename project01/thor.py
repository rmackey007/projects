#!/usr/bin/env python2.7

import os
import socket
import sys
import getopt
import logging
import time
import errno

# Global Variables

REQS = 1
PROCS = 1
VERB = False
PATH = '/'
ADDRESS = '127.0.0.1'
PORT    = '80'
PROGRAM = os.path.basename(sys.argv[0])
LOGLEVEL = logging.INFO

# Classes

# TCP Client Class
class TCPClient(object):
	def __init__(self, address, port):
		self.logger = logging.getLogger()
		self.address = socket.gethostbyname(address)
		self.port = port
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	def handle(self):
		self.stream.write('Hello\n')
		self.stream.flush()

	def run(self):
		self.logger.debug('connecting to {}:{}'.format(self.address, self.port))
		self.socket.connect((self.address, self.port))
		self.stream = self.socket.makefile('w+')
		self.handle()
		self.finish()

	def finish(self):
		self.socket.shutdown(socket.SHUT_RDWR)
		self.socket.close()

# HTTP Client Class
class HTTPClient(TCPClient):
	def __init__(self, url):
		self.logger = logging.getLogger()
		self.url = url.split('://')[-1]
		self.logger.debug('URL: {}'.format(self.url))
		# URL Parsing

		self.path = PATH
		self.address = ADDRESS
		self.port = PORT

		if '/'not in self.url:
			self.path = '/'
		else:
			self.path = '/' + self.url.split('/', 1)[-1]
		
		if '?' in self.path:
			self.path = path.split('?')[0]
			
		if ':' in self.url:
			self.address = self.url.split(':')[0]
			self.port = self.url.split(':')[-1]
			if '/' in self.port:
				self.port = self.port.split('/')[0]
		else:
			if '/' in self.url:
				self.address = self.url.split('/')[0]
			else:
				self.address = self.url
		
		self.port = int(self.port)
		self.host = self.address
		self.logger.debug('HOST: {}'.format(self.address))
		self.logger.debug('PORT: {}'.format(self.port))
		self.logger.debug('PATH: {}'.format(self.path))

		TCPClient.__init__(self, self.address, self.port)
	def handle(self):
		# Send request
		self.logger.debug('Handle')
		self.stream.write('GET {} HTTP/1.0\r\n'.format(self.path))
		self.stream.write('Host: {}\r\n'.format(self.host))
		self.stream.write('\r\n')
		self.logger.debug('Sending Request')
		self.stream.flush()
		
		# Receive response
		self.logger.debug('Receiving Response')
		data = self.stream.readline()
		while data:
			sys.stdout.write(data)
			data = self.stream.readline()
		
# Functions

def usage():
	print 'Usage: thor.py [-r REQS -p PROCS -v] URL \n \nOptions:\n\n\t-h\t\tShow this help message\n\t-v\t\tSett logging to DEBUG level\n\n\t-r REQS\tNumber of requests per process (default is 1)\n\t-p PROCS\tNumber of processes (default is 1)'
	return 1

# Get Command Line Arguments
try:
	options, args = getopt.getopt(sys.argv[1:], "r:p:v")
except getopt.GetoptError as e:
	usage()
	sys.exit(1)

for option, value in options:
	if option == "-r":
		REQS = int(value)
	elif option == "-p":
		PROCS = int(value)
	elif option == "-v":
		LOGLEVEL = logging.DEBUG
	else:
		usage()

# Set logging level
logging.basicConfig(
    level   = LOGLEVEL,
    format  = '[%(asctime)s] %(message)s',
    datefmt = '%Y-%m-%d %H:%M:%S',
)

	
# Thor

def main():
	
	URL = sys.argv[len(sys.argv)-1]
	times = []
	pid = 1		
	for i in range(PROCS):
		pid = os.fork()
		if pid == 0:
			for j in range(REQS):
				start_time = time.time()
				client = HTTPClient(URL)
				client.run()
				end_time = time.time()
				elapsed_time = end_time - start_time
				print 'Elapsed time: {:0.2f}'.format(elapsed_time)
				times.append(elapsed_time)
			#Calculate and display Average
			sum = float(0)
			for item in times:
				sum += item
			avg = sum / float(REQS)
			print 'Average elapsed time: {:0.2f}'.format(avg)
			
			sys.exit()
				
	for process in range(PROCS):
		os.wait()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
if __name__ == "__main__":
	main()
