CSE-20189-SP16 - Projects
=========================

This is the group repository for the final two projects in [CSE-20189-SP16].

Group Members
-------------

- Ryan Mackey				(rmackey1@nd.edu)
- Cat Elizabeth Badart			(cbadart@nd.edu)

[CSE-20189-SP16]: https://www3.nd.edu/~pbui/teaching/cse.20189.sp16/

1.	Hulk cracks passwords by iterating through every permutation of the alphabet of a specified length and comparing their hashes to our list of passwords.  Specifying a prefix is useful for the longer passwords (length 6 or longer) because those would take a really long time to check every iteration without itertools.  It was pretty easy to test hulk with passwords up through length 5 but for the longer ones we just made sure that it was finding passwords in with the right pattern and trusting that it would find all of them.

2.	Fury uses the prefix flag to split hulk into more tasks so that workers can work on it concurrently.  It calls on hulk with the default settings for password lengths 1-6 and then we used itertools to specify the prefixes for password lengths 7 and 8.  The log was actually really useful for debugging because we were able to figure out that the prefix values had commas at the end, which is why we weren't finding the 7 letter passwords.  The -p flag made it so that workers could work on 1336 tasks at once instead of 8.

3.	Longer passwords definitely are harder to crack than ones with a more complex alphabet.  Increasing the alphabet only increases the number of permutations by a factorial.  But increasing the length of the password increases them exponentially.
