#!/usr/bin/env python2.7

import hashlib
import itertools
import random
import string
import sys
import getopt

# Constants

ALPHABET = string.ascii_lowercase + string.digits
LENGTH   = int(8)
HASHES   = 'hashes.txt'
PREFIX = ''

# Utility Function

def md5sum(s):
    return hashlib.md5(s).hexdigest()

def usage():
	print 'Usage: hulk.py [-a ALPHABET -l LENGTH -s HASHES -p PREFIX] \n \nOptions:\n\n\t-h\t\tShow this help message\n\t-a ALPHABET\tSet desired alphabet\n\t-l LENGTH \tLength of permutations\n\t-s HASHES\tName of file containing hashes\n\t-p PREFIX\tInput a prefix for the permutations\n'
	return 1

# Get Command Line Arguments
try:
	options, args = getopt.getopt(sys.argv[1:], "a:l:s:p:")
except getopt.GetoptError as e:
	usage()
	sys.exit(1)

for option, value in options:
	if option == "-a":
		ALPHABET = value
	elif option == "-l":
		LENGTH = int(value)
	elif option == "-s":
		HASHES = value
	elif option == "-p":
		PREFIX = value
	else:
		usage()

# Main Execution

if __name__ == '__main__':

	LENGTH -= len(PREFIX)
	hashes = set([l.strip() for l in open(HASHES)])

	for candidate in itertools.product(ALPHABET, repeat=LENGTH):
		candidate = ''.join(candidate)
		candidate = PREFIX+candidate
		checksum  = md5sum(candidate)
		if checksum in hashes:
			print candidate
