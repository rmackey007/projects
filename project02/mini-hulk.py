#!/usr/bin/env python2.7

import hashlib
import random
import string
import sys

# Constants

ALPHABET = string.ascii_lowercase + string.digits
LENGTH   = int(sys.argv[1])
ATTEMPTS = int(sys.argv[2])
HASHES   = sys.argv[3]

# Utility Functions

def md5sum(s):
    return hashlib.md5(s).hexdigest()

# Main Execution

if __name__ == '__main__':
    hashes = set([l.strip() for l in open(HASHES)])
    found  = set()

    for attempt in range(ATTEMPTS):
        candidate = ''.join([random.choice(ALPHABET) for _ in range(LENGTH)])
        checksum  = md5sum(candidate)
        if checksum in hashes:
            found.add(candidate)

    for candidate in sorted(found):
        print candidate

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
