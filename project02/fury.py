#!/usr/bin/env python2.7

import sys
import work_queue
import json
import itertools
import os
import string

# Constants

JOURNAL  = {}
LENGTH   = int(sys.argv[1])
HASHES   = 'hashes.txt'
TASKS    = int(sys.argv[2])
SOURCES  = ('hulk.py', HASHES)
PORT     = int(sys.argv[3])

# Main Execution

if __name__ == '__main__':
    queue = work_queue.WorkQueue(PORT, name='fury-rmackey1', catalog=True)
    queue.specify_log('fury.log')

    for num in range(1, LENGTH+1):
        if num < 7:
            command = './hulk.py -l {}'.format(num)
            task    = work_queue.Task(command)

            for source in SOURCES:
                task.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)

            if command in JOURNAL:
                print >>sys.stderr, 'Already did', command
            else:
                queue.submit(task)

        if num == 7:
            for prefix in itertools.product(string.ascii_lowercase + string.digits):
                prefix = prefix[0]
                command = './hulk.py -l {} -p {}'.format(7, prefix)
                task    = work_queue.Task(command)

                for source in SOURCES:
                    task.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)

                if command in JOURNAL:
                    print >>sys.stderr, 'Already did', command
                else:
                    queue.submit(task)
        if num == 8:
            for candidate in itertools.product(string.ascii_lowercase + string.digits, repeat=2):
                prefix = ''.join(candidate)
                command = './hulk.py -l {} -p {}'.format(8, prefix)
                task    = work_queue.Task(command)

                for source in SOURCES:
                    task.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)

                if command in JOURNAL:
                    print >>sys.stderr, 'Already did', command
                else:
                    queue.submit(task)

    while not queue.empty():
        task = queue.wait()
        if task and task.return_status == 0:
            JOURNAL[task.command] = task.output.split()
            with open('journal.json.new', 'w') as stream:
                json.dump(JOURNAL, stream)
            os.rename('journal.json.new', 'journal.json')
        else:
            JOURNAL[task.command] = 'failed'
            with open('journal.json.new', 'w') as stream:
                json.dump(JOURNAL, stream)
            os.rename('journal.json.new', 'journal.json')
        